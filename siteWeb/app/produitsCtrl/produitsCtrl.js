(function() {
    angular
        .module('app')
        .controller('produits', ['$scope', '$http', 'produitSrvc' /*on injecte le service*/ ,
            function($scope, $http, produitSrvc) {
                $scope.produits = produitSrvc.produits;
                // console.log(produitSrvc.findProduit(1));

                $scope.ajouterProduit = produitSrvc.ajouterProduit;
            }
        ]);
})();