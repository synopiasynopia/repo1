// on recupere le module
(function() {

    angular.module('app').controller('produit', ['$scope', '$routeParams', '$http', 'produitSrvc',
        function($scope, $routeParams, $http, produitSrvc) {
            var vm = this;
            $scope.produit;
            if ($routeParams.id != undefined) {
                produitSrvc.getProduit($routeParams.id).then(function(reponse) {
                    // console.log(reponse);
                    if (reponse != undefined)
                        $scope.produit = reponse;
                });
            }
            // console.log($scope.produit);
            // $scope.titre = 'Airbus';
            // $scope.desc = 'Avion';
            // $scope.spec = [{ titre: 'vitesse max', val: '1000km/h' }, { titre: 'vitesse min', val: '5km/h' }];
            // $scope.ristourne = ['Employé : 10%', 'Employé : 10%', 'Familles : 10%'];
            // $scope.photo = 'img/A380.jpg';
            // $scope.prix = '10';

            $scope.canBeShown = function() {
                if ($scope.produit === undefined || $scope.produit.desc.length < 10)
                    return false;
                return true;
            };

            $scope.submit = function() {
                console.log("submit");
                if ($scope.produit.id != undefined) {
                    $http.patch('http://localhost:785/produits/' + $scope.produit.id, JSON.stringify($scope.produit))
                        .success(function(data, status, headers) {
                            $scope.ServerResponse = data;
                            console.log(data);
                        })
                        .error(function(data, status, header, config) {
                            $scope.ServerResponse = htmlDecode("Data: " + data +
                                "\n\n\n\nstatus: " + status +
                                "\n\n\n\nheaders: " + header +
                                "\n\n\n\nconfig: " + config);

                            console.log("ERROR:" + data);
                        });
                } else {
                    $http.post('http://localhost:785/produits/', JSON.stringify($scope.produit))
                        .success(function(data, status, headers) {
                            $scope.ServerResponse = data;
                            console.log(data);
                        })
                        .error(function(data, status, header, config) {
                            $scope.ServerResponse = htmlDecode("Data: " + data +
                                "\n\n\n\nstatus: " + status +
                                "\n\n\n\nheaders: " + header +
                                "\n\n\n\nconfig: " + config);

                            console.log("ERROR:" + data);
                        });
                }
            };

            $scope.ajouterSpec = function() {
                $scope.produit.spec[$scope.produit.spec.length] = { titre: "", spec: "" };
            }

            $scope.retirerSpec = function(i) {
                $scope.produit.spec.splice(i, 1);
            }
        }
    ]);
})();