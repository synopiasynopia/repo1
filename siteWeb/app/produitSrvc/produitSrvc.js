(function() {

    angular
        .module('app')
        .service('produitSrvc', ['$http', '$resource', function($http, $resource) {
            // on utilise this car on est dans un service qui a vocation a etre transvere contrairement a un controller ou on peut utiliser le scope de la page
            this.produits = [];
            this.panier = [];
            // on met this dans une var pour y avoir acces dans la fonction en dessous
            var mySrvc = this;

            this.findProduit = function(id) {
                for (var p of mySrvc.produits) {
                    if (p.id == id) {
                        return p;
                    }
                }
                // for (var i = 0; i < mySrvc.produits.length; i++) {
                //     // console.log(mySrvc.produits[i]);
                //     if (mySrvc.produits[i].id === id) {
                //         return mySrvc.produits[i];
                //     }
                // }
            }

            function getProduitsList() {
                $http({ method: 'get', url: 'http://localhost:785/produits' })
                    .then(function(reponse) {
                        // console.log(reponse);
                        reponse.data.map(function(toPush) { mySrvc.produits.push(toPush) });
                        // for (var i = 0; i < reponse.data.length; i++) {
                        //     mySrvc.produits.push(reponse.data[i]);
                        // }

                        // console.log(mySrvc.findProduit(2));
                    });
            }



            getProduitsList();
            // console.log(this.produits);

            this.produitFromResources = $resource('http://localhost:785/produits/:id', { id: '@id' });

            this.getProduit = function(id) {
                var ret = this.produitFromResources.get({ id: id });
                return ret.$promise;
            };
            // this.getProduit(1).then(function(p) {
            //     console.log(p);
            // });

            this.ajouterProduit = function(produit) {
                for (let e of mySrvc.panier) {
                    if (e.produit.id === produit.id) {
                        e.quantite++;
                        return;
                    }
                }
                mySrvc.panier.push({ produit: produit, quantite: 1 });
            }
        }]);

})();