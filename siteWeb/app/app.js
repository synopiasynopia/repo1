(function() {
    angular.module('app', ['ngRoute', 'ngResource']).config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({ redirectTo: '/home' })
            .when('/produits', {
                templateUrl: 'vues/produits.html',
                controller: 'produits',
                controllerAs: 'produitsController'
            })
            .when('/produit', {
                templateUrl: 'vues/produit.html'
            })
            .when('/produit/:id', {
                templateUrl: 'vues/produit.html'
            })
            .when('/formProduit', {
                templateUrl: 'vues/formProduit.html',
                controller: 'produit',
                controllerAs: 'produitController'
            })
            .when('/formProduit/:id', {
                templateUrl: 'vues/formProduit.html',
                controller: 'produit',
                controllerAs: 'produitController'
            })
            .when('/home', {
                template: '<h2>Bonjour et bienvenue sur ce nouveau site de vente d&apos;avions en ligne</h2>'
            });
    }]);
})();

(function() {
    'use strict';
    angular
        .module('app')
        .filter('currency', function() {
            return FilterFilter;

            ////////////////

            function FilterFilter(params) {
                return params + '€';
            }
        });
})();

(function() {
    'use strict';
    angular
        .module('app')
        .filter('round', function() {
            return FilterFilter;

            ////////////////

            function FilterFilter(params, nb) {
                if (nb == undefined) nb = 2;
                var exp = Math.pow(10, nb);
                return Math.round((exp * params)) / exp;
            }
        });
})();