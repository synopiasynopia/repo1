(function() {
    'use strict';

    angular
        .module('app')
        .directive('monPanier', [function() {
            // Usage:
            //
            // Creates:
            //
            var directive = {

                controller: 'panier',
                templateUrl: 'app/panierDirective/panier.html',
                restrict: 'E',

            };
            return directive;
        }]);

})();