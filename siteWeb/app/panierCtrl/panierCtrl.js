(function() {

    angular
        .module('app')
        .controller('panier', ['$scope', '$routeParams', '$location', 'produitSrvc',

            function($scope, $routeParams, $location, produitSrvc) {
                // $scope.currentPath = $location.path;
                // console.log($scope.currentPath);
                $scope.panier = produitSrvc.panier;

                // var produit0 = { titre: 'tutu', prix: '900', photo: 'img/A380.jpg' };
                // var element0 = { produit: produit0, quantite: 1 }
                // $scope.panier.push(element0);

                // produitSrvc.getProduit($routeParams.id).then(function(reponse) {
                //     // console.log(reponse);
                //     // $scope.produit1 = reponse;
                //     // $scope.produits.push($scope.produit1);
                // });

                $scope.calculerTotal = function() {
                    var total = 0;
                    for (let e of $scope.panier) {
                        total += e.quantite * e.produit.prix;
                    }
                    return total;
                }

                $scope.retirerProduit = function(p) {
                    for (var i = 0; i < $scope.panier.length; i++) {
                        if ($scope.panier[i].produit.id === p.id) {
                            $scope.panier[i].quantite--;
                            if ($scope.panier[i].quantite == 0) {
                                $scope.panier.splice(i, 1);
                            }
                            return;
                        }
                    }
                    // for (let e of $scope.panier) {
                    //     if (e.produit.id === p.id) {
                    //         e.quantite--;
                    //         return;
                    //     }
                    // }
                }


            }
        ]);

})();